﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Tetrominoes
{
    public enum TetrominoesEnum
    {
        I,
        J,
        L,
        O,
        S,
        T,
        Z
    }
}
