﻿namespace Assets.Scripts.Tetrominoes
{
    public enum TetrominoRotation
    {
        Initial = 0,
        Right = 1,
        Twice = 2,
        Left = 3
    }
}
