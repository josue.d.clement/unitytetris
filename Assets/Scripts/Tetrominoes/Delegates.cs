﻿using UnityEngine;

namespace Assets.Scripts.Tetrominoes
{
    public delegate GameObject NewTileDelegate(Color color);
}
